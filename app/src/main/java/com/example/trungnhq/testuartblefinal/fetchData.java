package com.example.trungnhq.testuartblefinal;


import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class fetchData extends AsyncTask<Void, Void, String> {

    @Override
    protected String doInBackground(Void... voids) {
        String data = "";
        JSONObject kien = null;
        JSONArray kienArray = null;

        try {
            URL url = new URL("https://iot-agriculture.herokuapp.com/api/activity?from=2018-9-26&to=2018-9-27");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = "";
            while (true){
                line = bufferedReader.readLine();
                if(line == null) break;
                data += line;
            }

            kienArray = new JSONArray(data);
            kien = kienArray.getJSONObject(0);
            return (String) kien.get("activity");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "fail to get data from server";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        MainActivity.txtFetchedData.setText(s);
    }
}