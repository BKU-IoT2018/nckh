package com.example.trungnhq.testuartblefinal;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.app.Activity;
import android.view.Window;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.content.Intent;
import android.app.Dialog;
import android.content.DialogInterface;
import android.widget.TextView;
/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SelectionActivity extends Activity {
    MediaPlayer song_selection;
    MediaPlayer song_watering;
    MediaPlayer song_prune;
    MediaPlayer song_enve;
    MediaPlayer song_harvest;
    MediaPlayer song_cancel;
    MediaPlayer song_success;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        song_selection = MediaPlayer.create(this,R.raw.selection1);
        song_watering = MediaPlayer.create(this,R.raw.watering);
        song_prune = MediaPlayer.create(this,R.raw.prune);
        song_enve = MediaPlayer.create(this,R.raw.enve);
        song_harvest = MediaPlayer.create(this,R.raw.harvest);
        song_success = MediaPlayer.create(SelectionActivity.this, R.raw.success);
        song_cancel = MediaPlayer.create(SelectionActivity.this, R.raw.cancel);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);
        Button mReturnBtn = (Button) findViewById(R.id.return_home_button);
        song_selection.start();
        mReturnBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                goWelcomeActivity(v);
                song_selection.stop();
            }
        });

        Button mFertilizeBtn = (Button) findViewById(R.id.fertilize_button);
        mFertilizeBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                goFertilizeActivity(v);
                song_selection.stop();
            }
        });
        Button mSprayBtn = (Button) findViewById(R.id.spray_button);
        mSprayBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                //goFertilizeActivity(v);
                goSprayActivity(v);
                song_selection.stop();
            }
        });
        Button mWateringBtn = (Button) findViewById(R.id.watering_button);
        mWateringBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                showQuestionDialog("Bạn Xác Nhận Vừa Tưới Nước?", "2");
                song_selection.stop();
                song_watering.start();
            }
        });
        Button mPruneBtn = (Button) findViewById(R.id.prune_button);
        mPruneBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                showQuestionDialog("Bạn Xác Nhận Vừa Tỉa Cành?", "4");
                song_selection.stop();
                song_prune.start();
            }
        });
        Button mEnveBtn = (Button) findViewById(R.id.enve_button);
        mEnveBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                showQuestionDialog("Bạn Xác Nhận Vừa Bao Trái?", "5");
                song_selection.stop();
                song_enve.start();
            }
        });
        Button mHarvestBtn = (Button) findViewById(R.id.harvest_button);
        mHarvestBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                showQuestionDialog("Bạn Xác Nhận Vừa Thu Hoạch?", "6");
                song_selection.stop();
                song_harvest.start();
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    private void goWelcomeActivity (View v) {
        Intent intent = new Intent (this, WelcomeActivity.class);
        startActivity (intent);
        overridePendingTransition(R.anim.anim_12, R.anim.anim_23);
    }

    private void goFertilizeActivity (View v) {
        Intent intent = new Intent (this, FertilizeActivity.class);
        startActivity (intent);
        overridePendingTransition(R.anim.anim_32, R.anim.anim_21);
    }
    private void goSprayActivity(View v){
        Intent intent = new Intent (this, SprayActivity.class);
        startActivity (intent);
        overridePendingTransition(R.anim.anim_32, R.anim.anim_21);
    }
    private void showQuestionDialog (String message, final String activity) {
        final Dialog myDialog = new Dialog(this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.confirm_dialog_layout);
        myDialog.setCancelable(false);

        TextView mTextMessage  = (TextView) myDialog.findViewById(R.id.dialog_message);
        mTextMessage.setText(message);

        Button mYesBtn = (Button) myDialog.findViewById(R.id.yes_button);
        Button mNoBtn = (Button) myDialog.findViewById(R.id.no_button);
        mNoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Do your code here
                song_success.stop();
                song_cancel.stop();
                song_enve.stop();
                song_harvest.stop();
                song_prune.stop();
                song_watering.stop();
                song_cancel.start();
                myDialog.dismiss();
            }
        });

        mYesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new HttpGetRequest().execute("https://glacial-temple-77441.herokuapp.com/activity?time=32&activity=" + activity);

                //Do your code here
                song_success.stop();
                song_cancel.stop();
                song_enve.stop();
                song_harvest.stop();
                song_prune.stop();
                song_watering.stop();
                song_success.start();
                myDialog.dismiss();
            }
        });


        myDialog.show();
        myDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.cancel();
                    return true;
                }
                return false;
            }
        });
    }
}